from django.shortcuts import render

# Create your views here.
from django.contrib.auth.models import User, Group
from backend.api.models import Blog
# from backend.api.models import Blog
from rest_framework import viewsets
from backend.api.serializers import UserSerializer, GroupSerializer, BlogSerializer


class UserViewSet(viewsets.ModelViewSet):
    """
    API endpoint that allows users to be viewed or edited.
    """
    queryset = User.objects.all().order_by('-date_joined')
    serializer_class = UserSerializer


class GroupViewSet(viewsets.ModelViewSet):
    """
    API endpoint that allows groups to be viewed or edited.
    """
    queryset = Group.objects.all()
    serializer_class = GroupSerializer

class BlogViewSet(viewsets.ModelViewSet):
    queryset = Blog.objects.all()
    serializer_class = BlogSerializer
