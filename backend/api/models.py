from django.db import models

# Create your models here.

class Blog(models.Model):
    blog_id = models.CharField(max_length=10)
    title = models.CharField(max_length=200)
    number = models.BigIntegerField()
