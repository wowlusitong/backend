# Generated by Django 2.0.3 on 2018-03-22 09:53

from django.db import migrations, models


class Migration(migrations.Migration):

    initial = True

    dependencies = [
    ]

    operations = [
        migrations.CreateModel(
            name='Blog',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('blog_id', models.CharField(max_length=10)),
                ('title', models.CharField(max_length=100)),
                ('number', models.BigIntegerField()),
            ],
        ),
    ]
